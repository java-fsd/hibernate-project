package com.classpath.model;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name="pets")
public class Pet {
	
	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String name;
	
	@ManyToOne
	@JoinColumn(name="owner_id", nullable = false)
	private Owner owner;
	
	@ManyToMany(mappedBy = "pets", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Vet> vets = new ArrayList<>();
	
	private Pet() {}
	
	public Pet(String name) {
		this.name= name;
	}
	
	public void setOwner(Owner owner) {
		this.owner = owner;
	}
	
	public Owner getOwner() {
		return this.owner;
	}
	
	//scaffolding code
	public void addVet(Vet vet) {
		this.vets.add(vet);
		vet.getPets().add(this);
	}

	@Override
	public String toString() {
		return "Pet [id=" + id + ", name=" + name + "]";
	}
	
	

}
