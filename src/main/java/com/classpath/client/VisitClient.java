package com.classpath.client;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.classpath.model.Owner;
import com.classpath.model.Pet;
import com.classpath.model.Vet;
import com.classpath.util.HibernateUtil;

public class VisitClient {
	
	public static void main(String[] args) {
		
		visitClinic();
	}

	
	private static void visitClinic() {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = null;
		try {
			session = sessionFactory.openSession();
			session.beginTransaction();
			Owner owner = new Owner("Ramesh", 32, "ramesh@gmail.com");
			
			Pet pet = new Pet("Scooby");
			owner.addPet(pet);
			Vet vet = new Vet("John-Doe", "General-Physician");
			
			pet.addVet(vet);
			session.persist(owner);
			
			session.persist(pet);
			
			session.getTransaction().commit();
		} catch(Exception exception) {
			System.out.println("exception :: "+ exception.getMessage());
		} finally {
			if(session != null) {
				session.close();
			}
		}
	}
}
