package com.classpath.client;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.classpath.model.Owner;
import com.classpath.model.Pet;
import com.classpath.util.HibernateUtil;

public class OwnerDBClient {
	
	public static void main(String[] args) {
		insertOwnersAndPets();
		fetchAllOwners();
		fetchOwnerByOwnerId(1);
		deleteOwnerByPet(1);
	}

	private static void deleteOwnerByPet(int ownerId) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = null;
		try {
			session = sessionFactory.openSession();
			session.beginTransaction();
			
			Owner owner = session.get(Owner.class, ownerId);
			session.remove(owner);
			session.getTransaction().commit();
		} catch(Exception exception) {
			System.out.println("exception :: "+ exception.getMessage());
		} finally {
			if(session != null) {
				session.close();
			}
		}
	}

	private static void fetchOwnerByOwnerId(int ownerId) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = null;
		try {
			session = sessionFactory.openSession();
			session.beginTransaction();
			
			Owner owner = session.get(Owner.class, ownerId);
			System.out.println("Fetching owner with id 1 ::::");
				
				System.out.println(owner);
				owner.getPet().forEach(pet -> System.out.println(pet));
			
			session.getTransaction().commit();
		} catch(Exception exception) {
			System.out.println("exception :: "+ exception.getMessage());
		} finally {
			if(session != null) {
				session.close();
			}
		}
	}

	private static void fetchAllOwners() {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = null;
		try {
			session = sessionFactory.openSession();
			session.beginTransaction();
			
			List<Owner> owners = session.createQuery("from Owner", Owner.class).list();
			System.out.println("Fetching all the owners ::::");
				
			for(Owner owner: owners) {
				System.out.println(owner);
			}
			System.out.println("Fetching all the owners ::::");
			
			session.getTransaction().commit();
		} catch(Exception exception) {
			System.out.println("exception :: "+ exception.getMessage());
		} finally {
			if(session != null) {
				session.close();
			}
		}
	}

	private static void insertOwnersAndPets() {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = null;
		try {
			session = sessionFactory.openSession();
			session.beginTransaction();
			Owner owner = new Owner("Ravish", 18, "ravish@gmail.com");
			Pet pet1 = new Pet("Snoopy");
			Pet pet2 = new Pet("kitty");
			Pet pet3 = new Pet("rocky");
			
			owner.addPet(pet1);
			owner.addPet(pet2);
			owner.addPet(pet3);
			
			session.persist(owner);
			session.getTransaction().commit();
		} catch(Exception exception) {
			System.out.println("exception :: "+ exception.getMessage());
		} finally {
			if(session != null) {
				session.close();
			}
		}
	}

}
