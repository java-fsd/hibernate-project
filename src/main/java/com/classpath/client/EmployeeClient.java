package com.classpath.client;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.classpath.model.Address;
import com.classpath.model.Employee;
import com.classpath.util.HibernateUtil;

public class EmployeeClient {

	public static void main(String[] args) {

		insertEmployee();

		findAllEmployees();
		
		findEmployeeByEmployeId(2);
		
		deleteEmployeeByEmployeeId(2);
	}

	private static void deleteEmployeeByEmployeeId(int empId) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = null;
		try {

			session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			Employee employee = session.get(Employee.class, empId);
			System.out.println("Employee to be deleted from the table:: "+ empId);
			System.out.println(employee);
			
			session.remove(employee);
			transaction.commit();
		} catch (Exception exception) {
			System.out.println(exception.getMessage());

		} finally {
			if (session != null) {
				session.close();
			}
		}
		
	}

	private static void findEmployeeByEmployeId(int empId) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = null;
		try {

			session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			Employee employee = session.get(Employee.class, empId);
			
			System.out.println("Employee with id : "+ empId+ " found::");
			System.out.println(employee);
			transaction.commit();
		} catch (Exception exception) {
			System.out.println(exception.getMessage());

		} finally {
			if (session != null) {
				session.close();
			}
		}
		
	}

	private static void findAllEmployees() {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = null;
		try {

			session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			List<Employee> employees = session.createQuery("from Employee", Employee.class).list();
			
			employees.forEach(emp -> System.out.println(emp));
			transaction.commit();
		} catch (Exception exception) {
			System.out.println(exception.getMessage());

		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	private static void insertEmployee() {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = null;
		try {
			Employee ramesh = new Employee("Ramesh", 45000);
			Address address1 = new Address("1st cross", "Bangalore", "KA");
			ramesh.addAddress(address1);
			
			
			Employee suresh = new Employee("Suresh", 15000);
			Address address2 = new Address("1st cross", "Chennai", "TN");
			suresh.addAddress(address2);
			
			Employee mohan = new Employee("Mohan", 35000);
			Address address3 = new Address("1st cross", "Bhopal", "MP");
			mohan.addAddress(address3);
			
			Employee vishnu = new Employee("Vishnu", 75000);
			Address address4 = new Address("1st cross", "Calicut", "KL");
			Address address5 = new Address("2st cross", "Cochin", "KL");
			Address address6 = new Address("2st cross", "Ernakulam", "KL");
			
			vishnu.addAddress(address4);
			vishnu.addAddress(address5);
			vishnu.addAddress(address6);

			List<Employee> employees = new ArrayList<>();
			employees.add(ramesh);
			employees.add(suresh);
			employees.add(mohan);
			employees.add(vishnu);

			session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			for (Employee emp : employees) {
				session.persist(emp);
			}
			transaction.commit();
		} catch (Exception exception) {
			System.out.println(exception.getMessage());

		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
}
