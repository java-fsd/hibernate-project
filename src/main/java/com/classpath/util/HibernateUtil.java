package com.classpath.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.classpath.model.Address;
import com.classpath.model.Employee;
import com.classpath.model.Owner;
import com.classpath.model.Pet;
import com.classpath.model.Vet;

public class HibernateUtil {
	
	
	private static SessionFactory sessionFactory = null;
	
	public static SessionFactory getSessionFactory() {
		
		if(sessionFactory == null) {
			sessionFactory = new Configuration().configure("hibernate-cfg.xml")
					.addAnnotatedClass(Owner.class)
					.addAnnotatedClass(Employee.class)
					.addAnnotatedClass(Address.class)
					.addAnnotatedClass(Pet.class)
					.addAnnotatedClass(Vet.class)
					.buildSessionFactory();
		}
		return sessionFactory;
	}

}
